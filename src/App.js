import React from 'react'
// import Navigation from './routes'
// import CliendLayaout  from './layouts'
import { Navigation } from './routes'
import { CliendLayaout } from './layouts'

export default function App() {
  return (
      <CliendLayaout className="app">
        <h1 className='app__title'>Hola Mundo</h1>
        <Navigation />
      </CliendLayaout >
  )
}

